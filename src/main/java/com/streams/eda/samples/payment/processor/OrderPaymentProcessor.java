package com.streams.eda.samples.payment.processor;

import com.streams.samples.*;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Slf4j
@EnableBinding(OrderPaymentProcessor.StreamProcessor.class)
public class OrderPaymentProcessor {
    @StreamListener
    @SendTo({"orderInvoiced", "orderWorkflowStepCompleted"})
    public KStream<?, ?>[] process(@Input("orderPlaced") KStream<OrderKey, OrderPlaced> orderPlacedStream) {
        KStream<InvoiceKey, OrderInvoiced> orderInvoicedStream = orderPlacedStream.map(this::invoiceOrder);
        KStream<OrderKey, OrderWorkflowStepCompleted> workflowStepCompletedStream = orderPlacedStream
                .mapValues(this::toWorkflowStepCompleted);

        return new KStream[] {orderInvoicedStream, workflowStepCompletedStream};
    }

    private OrderWorkflowStepCompleted toWorkflowStepCompleted(OrderPlaced value) {
        return new OrderWorkflowStepCompleted("order-invoiced", "order-placed",
                currentTime());
    }

    private KeyValue<InvoiceKey, OrderInvoiced> invoiceOrder(OrderKey orderKey, OrderPlaced orderPlaced) {
        var invoiceKey = new InvoiceKey(UUID.randomUUID().toString());
        var invoiceTotal = List.ofAll(orderPlaced.getOrderItems())
                .map(i -> BigDecimal.valueOf(i.getQuantity() * getItemPrice(i.getId())))
                .foldLeft(BigDecimal.ZERO, BigDecimal::add);

        return KeyValue.pair(invoiceKey,
                new OrderInvoiced(orderKey, currentTime(), invoiceTotal.toString()));
    }

    private long currentTime() {
        return Instant.now().toEpochMilli();
    }

    private double getItemPrice(String itemId) {
        return itemId.hashCode() % 2019;
    }

    public interface StreamProcessor {
        @Input("orderPlaced")
        KStream<?, ?> orderPlaced();

        @Output("orderInvoiced")
        KStream<?, ?> orderInvoiced();

        @Output("orderWorkflowStepCompleted")
        KStream<?, ?> orderWorkflowStepCompleted();
    }
}
